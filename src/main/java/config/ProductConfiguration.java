package config;

import com.github.javafaker.Faker;
import models.Product;

import java.util.Locale;
import java.util.Random;

public class ProductConfiguration {
    private static final Faker faker = new Faker(new Locale("pt-BR"));

    public static String getProductEndpoint() {
        return BaseURL.getBaseUrl() + "/produtos";
    }

    public static Product getRandomProduct() {
        Random random = new Random();
        String nome = faker.commerce().productName(); // Nome de produto gerado pelo Java Faker
        int preco = random.nextInt(1000); // Preço aleatório
        String descricao = faker.lorem().sentence(); // Descrição gerada pelo Java Faker
        int quantidade = random.nextInt(100); // Quantidade aleatória

        return new Product(nome, preco, descricao, quantidade);
    }
    public static Product getUpdatedProductWithIncreasedPrice(Product originalProduct) {
        Random random = new Random();
        int increasedPrice = originalProduct.getPreco() + random.nextInt(10000); // preço original por um valor aleatório até 10000

        return new Product(originalProduct.getNome(), increasedPrice, originalProduct.getDescricao(), originalProduct.getQuantidade());
    }

}
