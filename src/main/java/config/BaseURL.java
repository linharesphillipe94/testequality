package config;

public class BaseURL {
    private static final String BASE_URL = "https://serverest.dev";

    public static String getBaseUrl() {
        return BASE_URL;
    }

}
