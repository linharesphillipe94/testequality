package config;

import com.github.javafaker.Faker;

import java.text.Normalizer;
import java.util.Locale;

public class UserConfiguration {
    private static final Faker faker = new Faker(new Locale("pt-BR"));

    public static String getRandomName() {
        return faker.name().fullName();
    }

    public static String getRandomEmailBasedOnName(String name) {
        return sanitizeName(name) + "@example.com";
    }

    public static String getRandomPassword() {
        return faker.internet().password(8, 16);
    }

    public static boolean getRandomAdminStatus() {
        return true;
    }

    // Este método transforma "João Guilherme Marins" em "joao.guilherme.marins"
    private static String sanitizeName(String name) {
        String sanitized = Normalizer.normalize(name, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "")
                .toLowerCase()
                .replaceAll(" ", ".")
                .replaceAll("[^a-z.]", "");
        return sanitized;
    }
}




