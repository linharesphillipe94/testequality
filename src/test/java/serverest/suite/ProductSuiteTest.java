package serverest.suite;

import org.junit.jupiter.api.DisplayName;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.IncludeClassNamePatterns;
import org.junit.platform.suite.api.SelectClasses;

import org.junit.runner.RunWith;
import serverest.tests.LoginTest;
import serverest.tests.ProductsTest;
import serverest.tests.UserCreateTest;

@RunWith(JUnitPlatform.class)
@DisplayName("ServeRest ")
@SelectClasses({
        UserCreateTest.class,
        LoginTest.class,
        ProductsTest.class,
})
@IncludeClassNamePatterns({"^.*$"})
public class ProductSuiteTest {
}
