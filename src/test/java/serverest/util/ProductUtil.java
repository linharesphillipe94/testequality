package serverest.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import config.BaseURL;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import models.Product;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

public class ProductUtil {
    private final String token;
    private static final String BASE_URL = BaseURL.getBaseUrl();
    private static final String PRODUCT_ENDPOINT = BASE_URL + "/produtos";
    private static final String PRODUCT_ID_FILE = "src/test/resources/data/productId.json";

    public ProductUtil(String token) {
        this.token = token;
    }

    public Response createProduct(Object product) {
        return executeRequest("POST", PRODUCT_ENDPOINT, product);
    }

    public Response getProductById(String productId) {
        return executeRequest("GET", endpointWithId(productId), null);
    }

    public Response listProductsUsingId(String id) {
        return executeRequest("GET", endpointWithQueryParam("_id", id), null);
    }

    public Response updateProduct(String productId, Object updatedProduct) {
        return executeRequest("PUT", endpointWithId(productId), updatedProduct);
    }

    public Response deleteProduct(String productId) {
        return executeRequest("DELETE", endpointWithId(productId), null);
    }

    public Response listAllProducts() {
        return executeRequest("GET", PRODUCT_ENDPOINT, null);
    }

    private Response executeRequest(String httpMethod, String url, Object body) {
        RequestSpecification request = given()
                .header("Authorization", token)
                .contentType("application/json");

        if (body != null) {
            request.body(body);
        }

        return switch (httpMethod) {
            case "GET" -> request.get(url);
            case "POST" -> request.post(url);
            case "PUT" -> request.put(url);
            case "DELETE" -> request.delete(url);
            default -> throw new IllegalArgumentException("Invalid HTTP method: " + httpMethod);
        };
    }

    private String endpointWithId(String productId) {
        return PRODUCT_ENDPOINT + "/" + productId;
    }

    private String endpointWithQueryParam(String param, String value) {
        return PRODUCT_ENDPOINT + "?" + param + "=" + value;
    }

    public String handleAndExtractProductId(Response response, int expectedStatusCode, String successMessage) {
        printMessageOnSuccess(response, expectedStatusCode, successMessage);
        return response.path("_id");
    }

    public void assertProductCreated(Response response, Product expectedProduct) {
        Map<String, ?> product = response.path("$");
        assertProductDetails(product, expectedProduct);
    }

    public void assertProductStructure(Map<String,?> product) {
        assertTrue(product.containsKey("nome"));
        assertTrue(product.containsKey("preco"));
        assertTrue(product.containsKey("descricao"));
        assertTrue(product.containsKey("quantidade"));
        assertTrue(product.containsKey("_id"));
    }

    private void assertProductDetails(Map<String, ?> actualProduct, Product expectedProduct) {
        assertTrue(expectedProduct.getNome().equals(actualProduct.get("nome")));
        assertEquals(expectedProduct.getPreco(), ((Number) actualProduct.get("preco")).intValue());
        assertTrue(expectedProduct.getDescricao().equals(actualProduct.get("descricao")));
        assertEquals(expectedProduct.getQuantidade(), ((Number) actualProduct.get("quantidade")).intValue());
    }


    private void printMessageOnSuccess(Response response, int expectedStatusCode, String successMessage) {
        if (response.getStatusCode() == expectedStatusCode && successMessage != null) {
            System.out.println(successMessage);
        }
    }

    public void saveProductIdToFile(String productId) {
        saveToFile(PRODUCT_ID_FILE, new HashMap<>() {{
            put("productId", productId);
        }});
    }

    public String getProductIdFromFile() {
        Map<String, String> map = readFromFile(PRODUCT_ID_FILE, Map.class);
        return map.get("productId");
    }

    private <T> T readFromFile(String filePath, Class<T> valueType) {
        try {
            return new ObjectMapper().readValue(new File(filePath), valueType);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void saveToFile(String filePath, Object data) {
        try {
            new ObjectMapper().writeValue(new File(filePath), data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Response executeAndAssert(Supplier<Response> apiCall, int expectedStatusCode) {
        try {
            Response response = apiCall.get();
            assertEquals(expectedStatusCode, response.getStatusCode());
            return response;
        } catch (Exception e) {
            fail("Test failed due to an unexpected exception: " + e.getMessage());
            return null;
        }
    }

    public void assertProductsEqual(Product expected, Product actual) {
        assertEquals(expected.getNome(), actual.getNome());
        assertEquals(expected.getPreco(), actual.getPreco());
        assertEquals(expected.getDescricao(), actual.getDescricao());
        assertEquals(expected.getQuantidade(), actual.getQuantidade());
    }
    public Response deleteProductWithInvalidToken(String productId) {
        return given()
                .header("Authorization", "Bearer INVALID_TOKEN") // Use um token inválido aqui.
                .contentType("application/json")
                .delete(endpointWithId(productId));
    }


}






