package serverest.tests;

import config.ProductConfiguration;
import io.restassured.response.Response;
import models.Product;
import org.junit.jupiter.api.*;
import serverest.util.ProductUtil;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ProductsTest {
    private ProductUtil apiUtils;
    private final String token = new LoginTest().getAuthorizationToken();
    private String productId;

    @BeforeEach
    public void setup() {
        apiUtils = new ProductUtil(token);
        productId = apiUtils.getProductIdFromFile();
    }

    @Test
    @Order(1)
    public void testListAllProducts() {
        Response response = apiUtils.executeAndAssert(apiUtils::listAllProducts, 200);
        List<Map<String, ?>> productList = response.path("produtos");
        productList.forEach(apiUtils::assertProductStructure);
    }

    @Test
    @Order(2)
    public void testCreateAndGetProduct() {
        Product product = ProductConfiguration.getRandomProduct();
        Response createResponse = apiUtils.executeAndAssert(() -> apiUtils.createProduct(product), 201);

        String createdProductId = apiUtils.handleAndExtractProductId(createResponse, 201, "Product created successfully.");
        apiUtils.saveProductIdToFile(createdProductId);

        Response getResponse = apiUtils.executeAndAssert(() -> apiUtils.getProductById(createdProductId), 200);
        apiUtils.assertProductCreated(getResponse, product);
    }

    @Test
    @Order(3)
    public void testGetProductById() {
        Response response = apiUtils.executeAndAssert(() -> apiUtils.getProductById(productId), 200);
        apiUtils.assertProductStructure(response.path("$"));
    }

    @Test
    @Order(4)
    public void testUpdateValidProduct() {
        Product originalProduct = apiUtils.executeAndAssert(() -> apiUtils.getProductById(productId), 200).as(Product.class);
        Product updatedProduct = ProductConfiguration.getUpdatedProductWithIncreasedPrice(originalProduct);
        apiUtils.executeAndAssert(() -> apiUtils.updateProduct(productId, updatedProduct), 200);

        Product fetchedUpdatedProduct = apiUtils.executeAndAssert(() -> apiUtils.getProductById(productId), 200).as(Product.class);
        apiUtils.assertProductsEqual(updatedProduct, fetchedUpdatedProduct);
    }

    @Test
    @Order(5)
    public void testSuccessfulProductDeletion() {
        Response response = apiUtils.deleteProduct(productId);
        assertEquals(200, response.getStatusCode());
        String message = response.path("message");
        assertTrue(message.equals("Registro excluído com sucesso") || message.equals("Nenhum registro excluído"), "Mensagem inesperada para status 200.");
    }

    @Test
    @Order(7)
    public void testDeleteProductWithInvalidOrMissingToken() {
        Response response = apiUtils.deleteProductWithInvalidToken(productId); // Método "deleteProductWithInvalidToken" precisa ser implementado em "ProductUtil"
        assertEquals(401, response.getStatusCode());
        assertEquals("Token de acesso ausente, inválido, expirado ou usuário do token não existe mais", response.path("message"));
    }


    @Test
    @Order(8)
    public void testListProductsUsingInvalidId() {
        Response response = apiUtils.listProductsUsingId("INVALID_ID");
        assertEquals(200, response.getStatusCode());
        List<Map<String, ?>> productList = response.path("produtos");
        assertTrue(productList.isEmpty(), "Expected an empty product list for invalid _id.");
    }

    @Test
    @Order(9)
    public void testCreateProductWithExistingName() {
        Product product = ProductConfiguration.getRandomProduct();
        apiUtils.createProduct(product); // Primeira criação

        Response response = apiUtils.createProduct(product); // Tentativa de criação duplicada
        assertEquals(400, response.getStatusCode());
        assertEquals("Já existe produto com esse nome", response.path("message"));
    }
    @Test
    @Order(10)
    public void testGetProductWithInvalidId() {
        Response response = apiUtils.getProductById("INVALID_ID");
        assertEquals(400, response.getStatusCode(), "Expected status code 400 for an invalid product ID");
        assertEquals("Produto não encontrado", response.path("message"), "Expected error message for invalid product ID");
    }

    @Test
    @Order(11)
    public void testUpdateProductWithExistingName() {
        try {
            // Criando um novo produto
            Product newProduct = ProductConfiguration.getRandomProduct();
            apiUtils.createProduct(newProduct);

            // Atualizando um produto existente com o nome do novo produto (que já existe)
            newProduct.setPreco(600); // Alterar o preço apenas para garantir alguma alteração
            Response response = apiUtils.updateProduct(productId, newProduct);

            // Validando a resposta
            assertEquals(400, response.getStatusCode());
            assertEquals("Já existe produto com esse nome", response.path("message"));

        } catch (Exception e) {
            fail("Test failed due to an unexpected exception: " + e.getMessage());
        }
    }

}








