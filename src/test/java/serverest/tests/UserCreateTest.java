package serverest.tests;

import com.fasterxml.jackson.annotation.JsonProperty;
import config.BaseURL;
import config.UserConfiguration;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.io.FileWriter;
import java.io.IOException;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserCreateTest {
    private static final String BASE_URL = BaseURL.getBaseUrl();
    private static final String USER_ENDPOINT = BASE_URL + "/usuarios";

    private static class User {
        @JsonProperty
        String nome;

        @JsonProperty
        String email;

        @JsonProperty
        String password;

        @JsonProperty
        String administrador;

        User(String nome, String email, String password, String administrador) {
            this.nome = nome;
            this.email = email;
            this.password = password;
            this.administrador = administrador;
        }

        @Override
        public String toString() {
            return "User {" +
                    "nome='" + nome + '\'' +
                    ", email='" + email + '\'' +
                    ", password='" + password + '\'' +
                    ", administrador='" + administrador + '\'' +
                    '}';
        }
    }

    @Test
    public void createUser() {
        String nome = UserConfiguration.getRandomName();
        String email = UserConfiguration.getRandomEmailBasedOnName(nome);
        String password = UserConfiguration.getRandomPassword();
        String adminStatus = String.valueOf(UserConfiguration.getRandomAdminStatus());

        User newUser = new User(nome, email, password, adminStatus);

        System.out.println("Creating user: " + newUser.toString());

        Response response = given()
                .contentType("application/json")
                .body(newUser)
                .when()
                .post(USER_ENDPOINT);

        assertEquals(201, response.getStatusCode(), "Usuário não foi criado corretamente");

        // Salva as credenciais dinâmicas em um arquivo JSON
        saveCredentialsToJSON(email, password);
    }

    private void saveCredentialsToJSON(String email, String password) {
        JSONObject credentials = new JSONObject();
        credentials.put("email", email);
        credentials.put("password", password);

        try (FileWriter file = new FileWriter("src/test/resources/data/credentials.json")) {
            file.write(credentials.toString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}





