package serverest.tests;

import config.BaseURL;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static io.restassured.RestAssured.given;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class LoginTest {
    private static final String BASE_URL = BaseURL.getBaseUrl();
    private static final String LOGIN_ENDPOINT = BASE_URL + "/login";

    private static String authorizationToken;
    private static long tokenTimestamp;

    private static class LoginRequest {
        private final String email;
        private final String password;

        public LoginRequest(String email, String password) {
            this.email = email;
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }

    public Response loginUser(String email, String password) {
        LoginRequest loginRequest = new LoginRequest(email, password);
        return given()
                .contentType("application/json")
                .body(loginRequest)
                .when()
                .post(LOGIN_ENDPOINT);
    }

    public String getAuthorizationToken() {
        if (authorizationToken == null || (System.currentTimeMillis() - tokenTimestamp) > 600 * 1000) {
            String[] credentials = getCredentialsFromJSON();
            Response response = loginUser(credentials[0], credentials[1]);
            if (response.getStatusCode() == 200 && response.path("authorization") != null) {
                authorizationToken = response.path("authorization");
                tokenTimestamp = System.currentTimeMillis();
            } else {
                System.out.println("Failed to retrieve authorization token. Status code: " + response.getStatusCode());
            }
        }
        return authorizationToken;
    }

    private String[] getCredentialsFromJSON() {
        String[] credentials = new String[2];
        try {
            String content = new String(Files.readAllBytes(Paths.get("src/test/resources/data/credentials.json")), StandardCharsets.UTF_8);
            JSONObject obj = new JSONObject(content);
            credentials[0] = obj.getString("email");
            credentials[1] = obj.getString("password");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return credentials;
    }

    @Test
    public void testSuccessfulLoginWithDynamicCredentials() {
        String[] credentials = getCredentialsFromJSON();
        Response response = loginUser(credentials[0], credentials[1]);
        assertTrue(response.path("message").toString().contains("Login realizado com sucesso"));
    }

    @Test
    public void testAuthorizationTokenIsReturnedWithDynamicCredentials() {
        getAuthorizationToken();
        assertTrue(authorizationToken.startsWith("Bearer "));
    }
}




