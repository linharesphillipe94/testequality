<h1>QA Desafio QualityMap </h1>



> Status do Projeto: :heavy_check_mark: (Concluído)



## Descrição do projeto

<p align="justify">

Este projeto é um desafio técnico que demonstra o uso de testes de API  utilizando RestAssured e Java. Ele inclui testes que envolve criação de usuário, autenticação, consulta,adição,modificação e exclusão de produto.
</p>

## Requisições

:heavy_check_mark: POST/usuarios (Criação de usuário como admin de forma dinâmica)

:heavy_check_mark: POST/login (Autenticação com usuário criado na requisição anterior)

:heavy_check_mark: GET/produtos (Listar todos os produtos cadastrados na API)

:heavy_check_mark: POST/produtos (Cadastrar um novo produto)

:heavy_check_mark: GET/produtos/{_id} (Buscar produto por ID)

:heavy_check_mark: PUT/produtos/{_id} (Editar produto por ID)

:heavy_check_mark: DELETE/produtos/{_id} (Excluir produto por ID)


## API testada:

> serverest.dev

## Estrutura do Projeto

O projeto está organizado de maneira modular separada pelas configurações de testes na 'main' e a parte dos Testes em 'test'

**main**: 

        BaseUrl: Classe de configuração da URL base.

        ProductConfiguration: Classe de configuração para geração 
        randômica e dinamica de produtos para os testes

        UserConfiguration: Classe de configuração para geração 
        de dados de usuários de forma randômica e dinamica

        Product: Classe com atributos para nome, preço, descrição
        e quantidade, fornecendo métodos para acessar e modificar esses valores.



**test**: 

        UserCreateTest: Classe que executa um teste para criar um novo usuário  
        gerando dados dinâmicos para o usuário e validando a resposta. Após a criação, 
        as credenciais do usuário são salvas em um arquivo JSON.

        LoginTest: Classe responsável por realizar testes de autenticação de usuário. 
        Ela contém métodos para efetuar login com um usuário, recuperar o token de autorização 
        e obter credenciais a partir de um arquivo JSON. Dois testes estão presentes: 
        um para verificar se o login é bem-sucedido com credenciais dinâmicas e 
        outro para assegurar que um token de autorização válido é retornado após o login.


        ProductsTest:  Classe responsável por executar testes do tópico de produtos. 
        Ela faz verificações de operações como listagem, criação, recuperação, atualização 
        e exclusão de produtos. Os testes também cobrem cenários negativos como a tentativa 
        de criar um produto com um nome que já existe e recuperação de produtos usando IDs 
        inválidos. Além de utilizar como auxílios a classe ProductUtil para facilitar
        as operações e validações.
        
        ProductUtil: Classe utilitária que fornece métodos  para operações da classe ProductTest.
        Ela facilita a criação, obtenção, atualização e exclusão de produtos e a listagem 
        de todos os produtos. Além disso, ela ajuda a gerenciar e validar a estrutura e
        os detalhes dos produtos. Também oferece funcionalidades para salvar e recuperar 
        IDs de produtos de um arquivo e executar requisições com tokens inválidos. 
        Essa classe serve como uma abstração para a execução dos testes e suas validações

        ProductSuiteTest: é uma suíte de testes que agrupa e executa especificamente os 
        testes definidos nas classes UserCreateTest, LoginTest e ProductsTest. 
        Utilizando o JUnit, a suíte permite que múltiplos testes sejam 
        executados juntos, facilitando a organização e execução conjunta de testes relacionados,
        aplicando o principio do CRUD quando executada.


        
## Estrategia

O plano de testes do projeto adota uma abordagem no modelo Page Objects, em que o  foco particular é 
no encapsulamento de configurações e endpoints. Isso é evidenciado pela modelagem de dados na classe 
"Product" e pelo uso de classes utilitárias, como aquelas que abstraem funcionalidades da "ProductTest", 
tornando o código mais organizado, limpo e fácil de manter.

Os testes são construídos seguindo um fluxo CRUD. Iniciam-se criando um usuário e, em seguida,
usando esse usuário para fazer login. Depois, busca-se a lista de todos os produtos cadastrados.
Esse mesmo princípio é aplicado ao ciclo de vida de um produto: cria-se um produto, armazena-se seu ID, 
consulta-se esse produto pelo ID, atualiza-se e, finalmente, exclui-se.


Então além do fluxo válido com validações do status code mas também o corpo da resposta e implementando 
tratamentos para exceções existe uma maior cobertura com os cenários negativos. 


## Pré-requisitos

:warning: [Java Development Kit (JDK17)]

:warning: [Maven]

:warning: [Git]

:warning: [JUnit5]


## Como rodar a aplicação :arrow_forward:

No terminal, clone o projeto:

```
git clone https://gitlab.com/linharesphillipe94/testequality.git

```

Acesse a pasta do projeto no terminal/cmd:

```
cd testequality
```

Instale as dependecias:

```
mvn clean install
```


## Como rodar os testes

Executar o teste da aplicação:

```
mvn test
```

## Linguagens utilizadas :books:

- [Java](https://pt-br.reactjs.org/docs/create-a-new-react-app.html)

## Desenvolvedores/Contribuidores :octocat:

| [<img src="https://avatars.githubusercontent.com/u/63056300?v=4" width=115><br><sub>Phillipe Linhares</sub>]
| :---: |



## Licença

The [MIT License]

Copyright :copyright: 2023 - QA Phillipe
